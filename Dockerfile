FROM node:14-alpine as builder
WORKDIR /app

RUN apk --no-cache add --virtual builds-deps build-base python

COPY package.json ./package.json
COPY yarn.lock ./yarn.lock

RUN yarn install --frozen-lockfile --production
# RUN npm rebuild bcrypt --build-from-source

RUN mkdir dist && cp -r node_modules ./dist/node_modules

RUN yarn

COPY src ./src
COPY tsconfig.json ./tsconfig.json
COPY next-env.d.ts ./next-env.d.ts
COPY next.config.js ./next.config.js

COPY .env ./.env
RUN yarn build

# ------------------
FROM alpine:latest as release
RUN apk add --no-cache nodejs
ENV NODE_ENV production
WORKDIR /app
COPY --from=builder /app/dist/node_modules ./node_modules
COPY --from=builder /app/.next ./.next
COPY --from=builder /app/.env ./.env
RUN echo 'node node_modules/.bin/next start -p 80' >| start.sh
EXPOSE 80
CMD ["sh", "start.sh"]
