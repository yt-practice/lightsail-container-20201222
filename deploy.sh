set -Ceu

mkdir ~/.aws || echo pass
echo '[default]' >| ~/.aws/credentials
echo "aws_access_key_id=$AWS_ACCESS_KEY_ID" >> ~/.aws/credentials
echo "aws_secret_access_key=$AWS_SECRET_ACCESS_KEY" >> ~/.aws/credentials
echo '[default]' >| ~/.aws/config
echo 'region=ap-northeast-1' >> ~/.aws/config
echo 'output=json' >> ~/.aws/config

docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY_IMAGE
docker pull $CI_REGISTRY_IMAGE:latest || true
docker build --cache-from $CI_REGISTRY_IMAGE:latest --tag $CI_REGISTRY_IMAGE:$CI_COMMIT_SHA --tag $CI_REGISTRY_IMAGE:latest .
docker push $CI_REGISTRY_IMAGE:$CI_COMMIT_SHA
docker push $CI_REGISTRY_IMAGE:latest

aws lightsail push-container-image --region ap-northeast-1 --service-name ${APP_SERVICE_NAME} --label api --image $CI_REGISTRY_IMAGE:$CI_COMMIT_SHA
aws lightsail get-container-images --service-name ${APP_SERVICE_NAME} | node scripts/make-container.js
aws lightsail create-container-service-deployment --service-name ${APP_SERVICE_NAME} --cli-input-json file://$(pwd)/container.json
