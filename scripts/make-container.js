const { promises: fs } = require('fs')
const path = require('path')

Promise.resolve()
	.then(async () => {
		const data = await fs.readFile('/dev/stdin', 'utf-8')
		const r = JSON.parse(data)
		const image = r.containerImages[0].image
		await fs.writeFile(
			path.join(__dirname, '../container.json'),
			JSON.stringify({
				containers: {
					api: {
						image,
						command: ['sh', 'start.sh'],
						environment: {},
						ports: {
							80: 'HTTP',
						},
					},
				},
				publicEndpoint: {
					containerName: 'api',
					containerPort: 80,
					healthCheck: {
						healthyThreshold: 2,
						unhealthyThreshold: 2,
						timeoutSeconds: 3,
						intervalSeconds: 5,
						path: '/api/health',
						successCodes: '200-499',
					},
				},
			}),
		)
	})
	.catch(x => {
		console.error(x)
		process.exit(1)
	})
