const path = require('path')
const basePath = ''
module.exports = {
	assetPrefix: basePath,
	publicRuntimeConfig: {
		basePath,
	},
	webpack(config, _options) {
		config.resolve.alias = {
			...config.resolve.alias,
			'~': path.resolve(__dirname, 'src'),
		}
		// config.node = {
		// 	...config.node,
		// 	fs: 'empty',
		// 	child_process: 'empty',
		// }
		return config
	},
}
