import type { FC } from 'react'

export const Default: FC = ({ children }) => {
	return <>{children}</>
}

export default Default
