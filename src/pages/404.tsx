import Default from '~/layouts/default'

const NotFound = () => {
	return (
		<Default>
			<h2>404 not found.</h2>
		</Default>
	)
}

export default NotFound
