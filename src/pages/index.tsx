import Default from '~/layouts/default'

const Index = () => (
	<Default>
		<h2>index</h2>
		<p>lightsail の container を使ってみるテスト</p>
	</Default>
)

export default Index
